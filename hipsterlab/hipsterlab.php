<?php
/*
Plugin Name: HipsterLab
Plugin URI: http://hipsterlab.com/
Description: HipsterLab Business Core
Version: 1.0.0
Author: HipsterLab
Author URI: http://hipsterlab.com/
License: GPLv2 or later
Text Domain: forex
*/
// Register Custom Post Type
function forex_post_type_static_block() {

	// Static Post Type
	$labels = array(
		'name'                  => _x( 'Static Blocks', 'Post Type General Name', 'forex' ),
		'singular_name'         => _x( 'Static Block', 'Post Type Singular Name', 'forex' ),
		'menu_name'             => __( 'Static Blocks', 'forex' ),
		'name_admin_bar'        => __( 'Static Block', 'forex' ),
		'archives'              => __( 'Static Block Archives', 'forex' ),
		'parent_item_colon'     => __( 'Parent Item:', 'forex' ),
		'all_items'             => __( 'All Items', 'forex' ),
		'add_new_item'          => __( 'Add New Item', 'forex' ),
		'add_new'               => __( 'Add New', 'forex' ),
		'new_item'              => __( 'New Item', 'forex' ),
		'edit_item'             => __( 'Edit Item', 'forex' ),
		'update_item'           => __( 'Update Item', 'forex' ),
		'view_item'             => __( 'View Item', 'forex' ),
		'search_items'          => __( 'Search Item', 'forex' ),
		'not_found'             => __( 'Not found', 'forex' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'forex' ),
		'featured_image'        => __( 'Featured Image', 'forex' ),
		'set_featured_image'    => __( 'Set featured image', 'forex' ),
		'remove_featured_image' => __( 'Remove featured image', 'forex' ),
		'use_featured_image'    => __( 'Use as featured image', 'forex' ),
		'insert_into_item'      => __( 'Insert into item', 'forex' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'forex' ),
		'items_list'            => __( 'Items list', 'forex' ),
		'items_list_navigation' => __( 'Items list navigation', 'forex' ),
		'filter_items_list'     => __( 'Filter items list', 'forex' ),
	);
	$args = array(
		'label'                 => __( 'Static Block', 'forex' ),
		'description'           => __( 'Static Block', 'forex' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-editor-kitchensink',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'static_block', $args );
	// Services Post Type
	$labels = array(
		'name'                  => _x( 'Services', 'Post Type General Name', 'forex' ),
		'singular_name'         => _x( 'Service', 'Post Type Singular Name', 'forex' ),
		'menu_name'             => __( 'Services', 'forex' ),
		'name_admin_bar'        => __( 'Service', 'forex' ),
		'archives'              => __( 'Services Archives', 'forex' ),
		'parent_item_colon'     => __( 'Parent Item:', 'forex' ),
		'all_items'             => __( 'All Items', 'forex' ),
		'add_new_item'          => __( 'Add New Item', 'forex' ),
		'add_new'               => __( 'Add New', 'forex' ),
		'new_item'              => __( 'New Item', 'forex' ),
		'edit_item'             => __( 'Edit Item', 'forex' ),
		'update_item'           => __( 'Update Item', 'forex' ),
		'view_item'             => __( 'View Item', 'forex' ),
		'search_items'          => __( 'Search Item', 'forex' ),
		'not_found'             => __( 'Not found', 'forex' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'forex' ),
		'featured_image'        => __( 'Featured Image', 'forex' ),
		'set_featured_image'    => __( 'Set featured image', 'forex' ),
		'remove_featured_image' => __( 'Remove featured image', 'forex' ),
		'use_featured_image'    => __( 'Use as featured image', 'forex' ),
		'insert_into_item'      => __( 'Insert into item', 'forex' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'forex' ),
		'items_list'            => __( 'Items list', 'forex' ),
		'items_list_navigation' => __( 'Items list navigation', 'forex' ),
		'filter_items_list'     => __( 'Filter items list', 'forex' ),
	);
	$args = array(
		'label'                 => __( 'Service', 'forex' ),
		'description'           => __( 'Service', 'forex' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail','excerpt' ),
		'hierarchical'          => false,
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-analytics',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
	);
	register_post_type( 'services', $args );
	// Testimonials
	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'forex' ),
		'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'forex' ),
		'menu_name'             => __( 'Testimonials', 'forex' ),
		'name_admin_bar'        => __( 'Testimonial', 'forex' ),
		'archives'              => __( 'Testimonials Archives', 'forex' ),
		'parent_item_colon'     => __( 'Parent Item:', 'forex' ),
		'all_items'             => __( 'All Items', 'forex' ),
		'add_new_item'          => __( 'Add New Item', 'forex' ),
		'add_new'               => __( 'Add New', 'forex' ),
		'new_item'              => __( 'New Item', 'forex' ),
		'edit_item'             => __( 'Edit Item', 'forex' ),
		'update_item'           => __( 'Update Item', 'forex' ),
		'view_item'             => __( 'View Item', 'forex' ),
		'search_items'          => __( 'Search Item', 'forex' ),
		'not_found'             => __( 'Not found', 'forex' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'forex' ),
		'featured_image'        => __( 'Featured Image', 'forex' ),
		'set_featured_image'    => __( 'Set featured image', 'forex' ),
		'remove_featured_image' => __( 'Remove featured image', 'forex' ),
		'use_featured_image'    => __( 'Use as featured image', 'forex' ),
		'insert_into_item'      => __( 'Insert into item', 'forex' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'forex' ),
		'items_list'            => __( 'Items list', 'forex' ),
		'items_list_navigation' => __( 'Items list navigation', 'forex' ),
		'filter_items_list'     => __( 'Filter items list', 'forex' ),
	);
	$args = array(
		'label'                 => __( 'Testimonial', 'forex' ),
		'description'           => __( 'Testimonial', 'forex' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail'),
		'hierarchical'          => false,
		'public'                => true,
		'publicly_queryable'    => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-testimonial',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
	);
	register_post_type( 'testimonials', $args );
	// Staff
	$labels = array(
		'name'                  => _x( 'Staff', 'Post Type General Name', 'forex' ),
		'singular_name'         => _x( 'Staff', 'Post Type Singular Name', 'forex' ),
		'menu_name'             => __( 'Staff', 'forex' ),
		'name_admin_bar'        => __( 'Staff', 'forex' ),
		'archives'              => __( 'Staff Archives', 'forex' ),
		'parent_item_colon'     => __( 'Parent Item:', 'forex' ),
		'all_items'             => __( 'All Items', 'forex' ),
		'add_new_item'          => __( 'Add New Item', 'forex' ),
		'add_new'               => __( 'Add New', 'forex' ),
		'new_item'              => __( 'New Item', 'forex' ),
		'edit_item'             => __( 'Edit Item', 'forex' ),
		'update_item'           => __( 'Update Item', 'forex' ),
		'view_item'             => __( 'View Item', 'forex' ),
		'search_items'          => __( 'Search Item', 'forex' ),
		'not_found'             => __( 'Not found', 'forex' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'forex' ),
		'featured_image'        => __( 'Featured Image', 'forex' ),
		'set_featured_image'    => __( 'Set featured image', 'forex' ),
		'remove_featured_image' => __( 'Remove featured image', 'forex' ),
		'use_featured_image'    => __( 'Use as featured image', 'forex' ),
		'insert_into_item'      => __( 'Insert into item', 'forex' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'forex' ),
		'items_list'            => __( 'Items list', 'forex' ),
		'items_list_navigation' => __( 'Items list navigation', 'forex' ),
		'filter_items_list'     => __( 'Filter items list', 'forex' ),
	);
	$args = array(
		'label'                 => __( 'Staff', 'forex' ),
		'description'           => __( 'Staff', 'forex' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'excerpt', 'editor', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
	);
	register_post_type( 'staff', $args );
	// Job
	$labels = array(
		'name'                  => _x( 'Jobs', 'Post Type General Name', 'forex' ),
		'singular_name'         => _x( 'Job', 'Post Type Singular Name', 'forex' ),
		'menu_name'             => __( 'Job', 'forex' ),
		'name_admin_bar'        => __( 'Jobs', 'forex' ),
		'archives'              => __( 'Jobs Archives', 'forex' ),
		'parent_item_colon'     => __( 'Parent Item:', 'forex' ),
		'all_items'             => __( 'All Items', 'forex' ),
		'add_new_item'          => __( 'Add New Item', 'forex' ),
		'add_new'               => __( 'Add New', 'forex' ),
		'new_item'              => __( 'New Item', 'forex' ),
		'edit_item'             => __( 'Edit Item', 'forex' ),
		'update_item'           => __( 'Update Item', 'forex' ),
		'view_item'             => __( 'View Item', 'forex' ),
		'search_items'          => __( 'Search Item', 'forex' ),
		'not_found'             => __( 'Not found', 'forex' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'forex' ),
		'featured_image'        => __( 'Featured Image', 'forex' ),
		'set_featured_image'    => __( 'Set featured image', 'forex' ),
		'remove_featured_image' => __( 'Remove featured image', 'forex' ),
		'use_featured_image'    => __( 'Use as featured image', 'forex' ),
		'insert_into_item'      => __( 'Insert into item', 'forex' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'forex' ),
		'items_list'            => __( 'Items list', 'forex' ),
		'items_list_navigation' => __( 'Items list navigation', 'forex' ),
		'filter_items_list'     => __( 'Filter items list', 'forex' ),
	);
	$args = array(
		'label'                 => __( 'Jobs', 'forex' ),
		'description'           => __( 'Company Job', 'forex' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'hierarchical'          => false,
		'public'                => true,
		'publicly_queryable'    => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'           	=> 'dashicons-id',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
	);
	register_post_type( 'jobs', $args );
}
add_action( 'init', 'forex_post_type_static_block', 0 );
// Widget
require_once plugin_dir_path(__FILE__) . '/widget/recent-post.php';

// Load the theme/plugin options
if ( file_exists( plugin_dir_path( __FILE__ ) . '/options-init.php' ) ) {
	require_once plugin_dir_path( __FILE__ ) . '/options-init.php';
}

// Load Redux extensions
if ( file_exists( plugin_dir_path( __FILE__ ) . '/redux-extensions/loader.php' ) ) {
	require_once plugin_dir_path( __FILE__ ) . '/redux-extensions/loader.php';
}
